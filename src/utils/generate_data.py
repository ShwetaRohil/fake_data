import timeit
import time
from faker import Faker

fake_data = Faker()


def generate_data():
    """Generates n number of records with fake data"""
    no_of_records = int(
        input("how many records of data you wish to generate?"))
    datalist = []
    for _ in range(no_of_records):
        datalist.append([
            fake_data.name(),
            fake_data.email(),
            fake_data.address(),
            fake_data.city(),
            fake_data.postcode()])
    return datalist
