import csv


def create_file(input_data):
    """ Creates csv file with list of rows provided

    args:
        input_data: list of list
        example_input_data: [['aa','bb','cc'],['1a','2b','1c']]
    """
    file_name = input(
        "enter the file name in which you want to store the data: ")
    with open(file_name, 'w') as csvfile:
        csv_fake_data = csv.writer(csvfile)
        csv_fake_data.writerows(input_data)
    return True
