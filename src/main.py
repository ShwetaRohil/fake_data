from utils.generate_data import generate_data
from utils.file_generator import create_file

# generate fake data
data = generate_data()

# write csv
file_status = create_file(data)
if file_status:
    print("File created successfully")
else:
    print("File creation failed")
